package controllers;

public class MainController {

    public static void main(String[] args) {

		InitializationController initialization = new InitializationController();

	    SimulationController simulationController = new SimulationController(initialization);

	    Thread thread = new Thread(simulationController);
	    thread.start();
    }
}
