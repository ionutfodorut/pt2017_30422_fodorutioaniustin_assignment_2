package views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Ioan on 04.04.2017.
 */
public class InitializationView extends JFrame {

    private static JTextField totalSimulationTime = new JTextField();
    private static JTextField minArrivalInterval = new JTextField();
    private static JTextField maxArrivalInterval = new JTextField();
    private static JTextField minServiceTime = new JTextField();
    private static JTextField maxServiceTime = new JTextField();
    private static JTextField nrOfClients = new JTextField();
    private static JTextField nrOfServers = new JTextField();

    private JLabel totalSimulationTimeLabel = new JLabel("Total Simulation time");
    private JLabel minArrivalIntervalLabel = new JLabel("Min. Arrival Interval");
    private JLabel maxArrivalIntervalLabel = new JLabel("Max. Arrival Interval");
    private JLabel minServiceTimeLabel = new JLabel("Min. Service Time");
    private JLabel maxServiceTimeLabel = new JLabel("Max. Service Time");
    private JLabel nrOfClientsLabel = new JLabel("Nr. of Clients");
    private JLabel nrOfServersLabel = new JLabel("Nr. of Servers");

    private JLabel mainLabel = new JLabel("Simulation Initialization", SwingConstants.CENTER);
    private JLabel message = new JLabel("Please complete all the fields!", SwingConstants.CENTER);

    private JButton start = new JButton("Start Simulation!");
    private JButton clear = new JButton("Clear");

    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();
    private JPanel panel4 = new JPanel();

    public InitializationView() {

        JPanel initPanel = new JPanel();
        initPanel.setLayout(new GridLayout(2, 2));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600, 600);

        panel1.setLayout(new GridLayout(7, 2));
        panel1.add(totalSimulationTimeLabel);
        panel1.add(totalSimulationTime);
        panel1.add(minArrivalIntervalLabel);
        panel1.add(minArrivalInterval);
        panel1.add(maxArrivalIntervalLabel);
        panel1.add(maxArrivalInterval);
        panel1.add(minServiceTimeLabel);
        panel1.add(minServiceTime);
        panel1.add(maxServiceTimeLabel);
        panel1.add(maxServiceTime);
        panel1.add(nrOfClientsLabel);
        panel1.add(nrOfClients);
        panel1.add(nrOfServersLabel);
        panel1.add(nrOfServers);
        panel1.setBorder(new EmptyBorder(20, 20,20,20));

        mainLabel.setBounds(100, 100, 25, 25);
        mainLabel.setFont(new Font(mainLabel.getFont().getName(), Font.BOLD, 20));
        panel2.setLayout(new GridLayout(3, 1));
        panel2.setBorder(new EmptyBorder(100, 20, 20, 20));
        panel2.add(mainLabel);

        panel3.setLayout(new GridLayout(3,1));
        panel3.setBorder(new EmptyBorder(100, 20, 20,20));
        panel3.add(start);

        panel4.setLayout(new GridLayout(2,1));
        panel4.setBorder(new EmptyBorder(20, 20, 20, 20));
        panel4.add(message);
        panel4.add(clear);

        initPanel.add(panel2);
        initPanel.add(panel3);
        initPanel.add(panel1);
        initPanel.add(panel4);

        this.add(initPanel);
    }

    public void clear(){

        totalSimulationTime.setText("");
        minArrivalInterval.setText("");
        maxArrivalInterval.setText("");
        minServiceTime.setText("");
        maxServiceTime.setText("");
        nrOfClients.setText("");
        nrOfServers.setText("");
        message.setText("Cleared.");

    }

    public void setMessage(String msg) {
        message.setText(msg);
    }

    public String getTotalSimulationTime() {
        return totalSimulationTime.getText();
    }

    public String getMinArrivalInterval() {
        return minArrivalInterval.getText();
    }

    public String getMaxArrivalInterval() {
        return maxArrivalInterval.getText();
    }

    public String getMinServiceTime() {
        return minServiceTime.getText();
    }

    public String getMaxServiceTime() {
        return maxServiceTime.getText();
    }

    public String getNrOfClients() {
        return nrOfClients.getText();
    }

    public String getNrOfServers() {
        return nrOfServers.getText();
    }

    public void addClearListener(ActionListener listenForClearButton){
        clear.addActionListener(listenForClearButton);
    }
    public void addStartListener(ActionListener listenForStartButton){
        start.addActionListener(listenForStartButton);
    }
}
