package utilities;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by John on 03.01.2017.
 */

public class NameFactory {
    public String generateName(){
        String[] firstNames = {"Ioan", "Catalin", "Simina", "Cristina", "Lisa", "Cosmin", "Mary", "Ovidiu", "Maria", "Alexandru", "Elon", "Victor", "Paul", "Amanda", "Stephen", "Elisabeth", "Justin", "Vasile"};
        String[] lastNames = {"Poenaru", "DaVinci", "Idu", "Stein", "Musk", "Fagetan", "Tamas", "Zete", "Nenu", "Ciolos", "Radu", "Dumitroaea", "Coroi", "Fodoruț", "Obama", "Hawking", "Leibniz"};
        int randomNum1 = ThreadLocalRandom.current().nextInt(0, firstNames.length);
        int randomNum2 = ThreadLocalRandom.current().nextInt(0, lastNames.length);
        return firstNames[randomNum1] + " " + lastNames[randomNum2];
    }
}
