package model;

/**
 * Created by Ioan on 03.04.2017.
 */

public class Client {

    private String name;
    private int id;
    private int timeOfArrival;
    private int serviceTime;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getTimeOfArrival() {
        return timeOfArrival;
    }
    public void setTimeOfArrival(int timeOfArrival) {
        this.timeOfArrival = timeOfArrival;
    }
    public int getServiceTime() {
        return serviceTime;
    }
    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Client(String name, int serviceTime) {
        this.name = name;
        this.serviceTime = serviceTime;
    }
}
