package model;

import utilities.ClientFactory;
import utilities.Time;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Ioan on 03.04.2017.
 */
public class Simulation implements Runnable {

    private static int absoluteTime;
    private static int totalWaitingTime;
    private static int totalFreeTime;
    private int totalSimulationTime;
    private int finalTime;
    private int peakTime;
    private int minArrivalInterval;
    private int maxArrivalInterval;
    private int minServiceTime;
    private int maxServiceTime;
    private int nrOfClients;
    private int nrOfServers;
    private int clientsServed;
    private int totalServiceTime;
    private int maxNrOfClients;
    private ArrayList<Server> servers;
    private ArrayList<Client> clients;
    private ClientFactory clientFactory;
    private Boolean exit = false;
    private static String log = "";

    public Simulation(int totalSimulationTime, int minArrivalInterval, int maxArrivalInterval, int minServiceTime, int maxServiceTime, int nrOfClients, int nrOfServers) {

        absoluteTime = Time.getTime();
        totalWaitingTime = 0;
        totalFreeTime = 0;
        totalServiceTime = 0;
        finalTime = absoluteTime + totalSimulationTime;
        this.totalSimulationTime = totalSimulationTime;
        this.minArrivalInterval = minArrivalInterval;
        this.maxArrivalInterval = maxArrivalInterval;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.nrOfClients = nrOfClients;
        this.nrOfServers = nrOfServers;
        this.servers = new ArrayList<>(nrOfServers);
        this.clients = new ArrayList<>(nrOfClients);
        this.clientFactory = new ClientFactory(minServiceTime, maxServiceTime);
        this.peakTime = 0;
        this.maxNrOfClients = 0;
        this.clientsServed = 0;

    }

    public int getFinalTime() {
        return finalTime;
    }
    public void setFinalTime(int finalTime) {
        this.finalTime = finalTime;
    }
    public static int getTotalWaitingTime() {
        return totalWaitingTime;
    }
    public int getTotalSimulationTime() {
        return totalSimulationTime;
    }
    public static int getAbsoluteTime() {
        return absoluteTime;
    }
    public static void updateAbsoluteTime(int absoluteTime) {
        Simulation.absoluteTime += absoluteTime;
    }
    public static void setTotalWaitingTime(int totalWaitingTime) {
        Simulation.totalWaitingTime = totalWaitingTime;
    }
    public static int getTotalFreeTime() {
        return totalFreeTime;
    }
    public static void setTotalFreeTime(int totalFreeTime) {
        Simulation.totalFreeTime = totalFreeTime;
    }
    public void setTotalSimulationTime(int totalSimulationTime) {
        this.totalSimulationTime = totalSimulationTime;
    }
    public int getNrOfClients() {
        return nrOfClients;
    }
    public void setNrOfClients(int nrOfClients) {
        this.nrOfClients = nrOfClients;
    }
    public int getNrOfServers() {
        return nrOfServers;
    }
    public void setNrOfServers(int nrOfServers) {
        this.nrOfServers = nrOfServers;
    }
    public ArrayList<Server> getServers() {
        return servers;
    }
    public String getLog() { return log; }
    public static void appendToLog(String l) { log = log + l; }
    private int getOptimalServer() {
        int min = 5, j = 0;
        for (int i = 0; i < servers.size(); i++) {
            Server s = servers.get(i);
            if (s.getQueueSize() < min) {
                min = s.getQueueSize();
                j = i;
            }
        }
        return j;
    }
    public void stopSimulation() {
        exit = true;
    }
    public Boolean isStopped() { return exit; }
    public String getResults() { return "Avarage waiting time: " + getAvgWaitingTime() + "\n" +
                                    "Peak time: " + Time.intToString(peakTime) + "\n" +
                                    "Avarage service time: " + getAvgServiceTime() + "\n" +
                                     "Total server free time: " + getTotalFreeTime();}

    public void generateClients() {
        for (int i = 0; i < nrOfClients; i++) {
            Client c = this.clientFactory.getClient();
            c.setId(i);
            clients.add(c);

            System.out.println(Time.intToString(absoluteTime) + " SIMULATION: Client " + c.getName() + ", with ID " + c.getId() + " generated!");
            appendToLog(Time.intToString(absoluteTime) + " SIMULATION: Client " + c.getName() + ", with ID " + c.getId() + " generated!\n");
        }
    }

    public void generateServers() {
        for (int i = 0; i < nrOfServers; i++) {
            servers.add(new Server(i));

            System.out.println(Time.intToString(absoluteTime) + " " +
                    "SIMULATION: Server " + servers.get(i).getName() + ", with ID " + servers.get(i).getId() + " generated!");
            appendToLog(Time.intToString(absoluteTime) + " " +
                    "SIMULATION: Server " + servers.get(i).getName() + ", with ID " + servers.get(i).getId() + " generated!\n");
        }
    }

    public void updateFreeTime() {
        totalFreeTime = 0;
        for (int i = 0; i < nrOfServers; i++) {
            totalFreeTime += servers.get(i).getFreeTime();
        }
    }

    public void updatePeakTime() {
        int tempMaxNrOfClients = 0;
        for (int i = 0; i < nrOfServers; i++) {
            tempMaxNrOfClients += servers.get(i).getQueueSize();
        }
        if (tempMaxNrOfClients > maxNrOfClients) {
            maxNrOfClients = tempMaxNrOfClients;
            peakTime = absoluteTime;
        }
    }

    public void updateWaitingTime() {
        totalWaitingTime = 0;
        for (int i = 0; i < nrOfServers; i++) {
            totalWaitingTime += servers.get(i).getWaitingTime();
        }
    }

    public void updateServiceTime() {
        totalServiceTime = 0;
        for (int i = 0; i < clientsServed; i++) {
            totalServiceTime += clients.get(i).getServiceTime();
        }
    }

    public void updateClientsServed() {
        clientsServed = 0;
        for (int i = 0; i < nrOfServers; i++) {
            clientsServed += servers.get(i).getClientsServed();
        }
    }

    public int getAvgWaitingTime() {
        if (clientsServed > 0) {
            return totalWaitingTime / clientsServed;
        } else {
            return 1;
        }
    }

    public int getAvgServiceTime() {
        if (clientsServed > 0) {
            return totalServiceTime / clientsServed;
        } else {
            return 1;
        }
    }

    @Override
    public void run() {

            System.out.println(Time.intToString(absoluteTime) + " SIMULATION: Started at " + Time.intToString(absoluteTime));
            appendToLog(Time.intToString(absoluteTime) + " SIMULATION: Started at " + Time.intToString(absoluteTime) + "\n");

            generateServers();
            generateClients();
            Thread thread[] = new Thread[nrOfServers];
            int k = 0;
            for (Server server : servers) {
                thread[k] = new Thread(server);
                thread[k].start();
                k++;
            }

            System.out.println(Time.intToString(absoluteTime) + " SIMULATION: Simulation should finish around " + Time.intToString(finalTime));
            appendToLog(Time.intToString(absoluteTime) + " SIMULATION: Simulation should finish around " + Time.intToString(finalTime) + "\n");
            int i = 0;
            for (i = 0; i < nrOfClients; i++) {
                if (!exit) {
                    if (absoluteTime <= finalTime) {
                        try {
                            int interval = ThreadLocalRandom.current().nextInt(minArrivalInterval, maxArrivalInterval);
                            Thread.sleep(interval * 1000);
                            Client c = clients.get(i);
                            c.setTimeOfArrival(absoluteTime);
                            int optimalServer = getOptimalServer();

                            System.out.println(Time.intToString(absoluteTime) + " SIMULATION: Optimal server " + servers.get(optimalServer).getId() + " has size " + servers.get(optimalServer).getQueueSize());
                            appendToLog(Time.intToString(absoluteTime) + " SIMULATION: Optimal server " + servers.get(optimalServer).getId() + " has size " + servers.get(optimalServer).getQueueSize() + "\n");

                            servers.get(optimalServer).assignClient(c);

                            System.out.println(Time.intToString(absoluteTime) + " SIMULATION: Client " + c.getName() + " with ID " + c.getId() + " chose server " + optimalServer);
                            appendToLog(Time.intToString(absoluteTime) + " SIMULATION: Client " + c.getName() + " with ID " + c.getId() + " chose server " + optimalServer + "\n");

                            updateClientsServed();
                            updatePeakTime();
                            updateWaitingTime();
                            updateServiceTime();
                            updateFreeTime();


                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {

                        System.out.println(Time.intToString(absoluteTime) + " SIMULATION: Shop closed!");
                        appendToLog(Time.intToString(absoluteTime) + " SIMULATION: Shop closed!\n");

                        for (int l = 0; l < nrOfServers; l++) {
                            while (servers.get(l).getQueueSize() != 0) {
                                try {
                                    System.out.println(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + servers.get(l).getId() +
                                            ":" + " Client " + servers.get(l).getQueue().peek().getName() + ", with ID " + servers.get(l).getQueue().peek().getId() +
                                            ", has been closed inside the shop and served for " + servers.get(l).getQueue().peek().getServiceTime() + " seconds after closing time.");
                                    appendToLog(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + servers.get(l).getId() +
                                            ":" + " Client " + servers.get(l).getQueue().peek().getName() + ", with ID " + servers.get(l).getQueue().peek().getId() +
                                            ", has been closed inside the shop and served for " + servers.get(l).getQueue().peek().getServiceTime() + " seconds after closing time.\n");

                                    servers.get(l).serveClient();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            servers.get(l).stopServer();
                        }
                        //updateFreeTime();
                        //System.out.println(totalFreeTime);
                        stopSimulation();

                        System.out.println(Time.intToString(Simulation.getAbsoluteTime()) + " SIMULATION: Stopped.");
                        appendToLog(Time.intToString(Simulation.getAbsoluteTime()) + " SIMULATION: Stopped.\n");

                        break;
                    }
                } else {
                    for (int l = 0; l < nrOfServers; l++) {
                        while (servers.get(l).getQueueSize() != 0){
                            try {
                                System.out.println(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + servers.get(l).getId() +
                                        ":" + " Client " + servers.get(l).getQueue().peek().getName() + ", with ID " + servers.get(l).getQueue().peek().getId() +
                                        ", has been closed inside the shop and served for " + servers.get(l).getQueue().peek().getServiceTime() + " seconds after closing time.");
                                appendToLog(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + servers.get(l).getId() +
                                        ":" + " Client " + servers.get(l).getQueue().peek().getName() + ", with ID " + servers.get(l).getQueue().peek().getId() +
                                        ", has been closed inside the shop and served for " + servers.get(l).getQueue().peek().getServiceTime() + " seconds after closing time.\n");

                                servers.get(l).serveClient();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        servers.get(l).stopServer();
                    }

                    System.out.println(totalFreeTime);
                    stopSimulation();
                    System.out.println(Time.intToString(Simulation.getAbsoluteTime()) + " SIMULATION: Stopped.");
                    appendToLog(Time.intToString(Simulation.getAbsoluteTime()) + " SIMULATION: Stopped.\n");
                    break;
                }
            }
    }
}
